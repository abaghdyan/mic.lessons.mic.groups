﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIC.Groups
{
    static class MockDataBase
    {
        public static List<Student> LoadStudents(int count)
        {
            if (count == 0)
            {
                return null;
            }
            var studList = new List<Student>(count);
            Random rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                Student student = new Student();
                student.Age = rnd.Next(16, 50);
                student.Name = $"S{i}";
                student.Surname = $"S{i}yan";
                student.Email = $"S{i}@gmail.com";
                studList.Add(student);
            }
            return studList;
        }

        public static List<Teacher> LoadTeachers(int count)
        {
            if (count == 0)
            {
                return null;
            }
            var teacherList = new List<Teacher>(count);
            Random rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                Teacher teacher = new Teacher();
                teacher.Name = $"T{i}";
                teacher.Surname = $"T{i}yan";
                teacherList.Add(teacher);
            }
            return teacherList;
        }
    }
}
