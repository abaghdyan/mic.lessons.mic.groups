﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIC.Groups
{
    class Program
    {
        static void Main(string[] args)
        {
            int studentsCount = 50;
            int teachersCount = 3;

            List<Student> studList = MockDataBase.LoadStudents(studentsCount);
            List<Teacher> teacherList = MockDataBase.LoadTeachers(teachersCount);

            studList.Shuffle();

            Print(studList);
            Print(teacherList);

            List<Group> groupList = MicManager.GetGroups(studList, teacherList);
            Print(groupList);

            Console.ReadLine();
        }

        private static void Print(List<Teacher> teacherList)
        {
            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.WriteLine($">>>>>>>>Teachers List<<<<<<<<");
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.Write($"Name\tSurname\n");
            Console.ResetColor();
            foreach (var item in teacherList)
            {
                Console.Write($"{item.Name}\t{item.Surname}\n");
            }
            Console.WriteLine("----------------------------");
            Console.WriteLine();
        }

        private static void Print(List<Student> studList)
        {
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine($">>>>>>>>Students List<<<<<<<<");
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.Write($"Name\tSurname\tAge\tEmail\n");
            Console.ResetColor();
            foreach (var item in studList)
            {
                Console.Write($"{item.Name}\t{item.Surname}\t{item.Age}\t{item.Email}\n");
            }
            Console.WriteLine("----------------------------");
            Console.WriteLine();
        }

        private static void Print(List<Group> groupList)
        {
            Console.BackgroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine($"**********MIC Groups**********");
            Console.ResetColor();
            Console.WriteLine();
            foreach (var item in groupList)
            {
                Console.WriteLine($"Group name: {item.Title}");
                Console.WriteLine($"Teacher name: {item.teacher.Name} {item.teacher.Surname}");
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Students list");
                Console.ResetColor();
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.Write($"Name\tSurname\tAge\tEmail\n");
                Console.ResetColor();
                foreach (var student in item.studList)
                {
                    Console.Write($"{student.Name}\t{student.Surname}\t{student.Age}\t{student.Email}\n");
                }
                Console.WriteLine("----------------------------");
                Console.WriteLine();
            }
        }
    }
}
