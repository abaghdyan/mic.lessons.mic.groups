﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIC.Groups
{
    class Group
    {
        public Group()
        {
            Title = "UnknownGroup";
        }
        private string _title;
        public string Title
        {
            get { return _title; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                    _title = value;
            }
        }

        public Teacher teacher;
        public List<Student> studList = new List<Student>();
    }
}
