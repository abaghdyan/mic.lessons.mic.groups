﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIC.Groups
{
    abstract class Person
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                    _name = value;
            }
        }

        private string _surname;
        public string Surname
        {
            get { return _surname; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                    _surname = value;
            }
        }
    }
}
