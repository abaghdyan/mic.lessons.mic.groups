﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIC.Groups
{
    static class MicManager
    {
        public static void Shuffle(this List<Student> student)
        {
            Random rnd = new Random();
            int n = student.Count;
            while (n > 1)
            {
                n--;
                int k = rnd.Next(n + 1);
                var strr = student[k];
                student[k] = student[n];
                student[n] = strr;
            }
        }

        public static List<Group> GetGroups(List<Student> sourceStudList, List<Teacher> sourceTeacherList)
        {
            if (sourceStudList == null || sourceTeacherList == null 
                || sourceStudList.Count==0 || sourceTeacherList.Count==0)
            {
                return null;
            }

            List<Student> studList = new List<Student>(sourceStudList);
            List<Teacher> teacherList = new List<Teacher>(sourceTeacherList);


            //List<Teacher> unemployedТeachList = null;

            //if (studList.Count < teacherList.Count)
            //{
            //    unemployedТeachList = new List<Teacher>(teacherList);
            //    teacherList.RemoveRange(studList.Count, teacherList.Count - studList.Count);
            //    unemployedТeachList.RemoveRange(0, teacherList.Count);
            //}

            var groupList = new List<Group>(teacherList.Count);
            for (int i = 0; i < teacherList.Count; i++)
            {
                groupList.Add(new Group());
            }

            int groupNumber = 0;

            foreach (Group item in groupList)
            {
                for (int i = 0; i < studList.Count / teacherList.Count; i++)
                {
                    item.studList.Add(studList[i]);
                }
                studList.RemoveRange(0, studList.Count / teacherList.Count);
                item.teacher = teacherList[0];
                item.Title = "C# OOP Fundamental: Group " + groupNumber++;
                teacherList.RemoveRange(0,1);
            }

            return groupList;
        }
    }
}
