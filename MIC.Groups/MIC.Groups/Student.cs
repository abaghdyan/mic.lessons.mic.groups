﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIC.Groups
{
    class Student : Person
    {
        public Student()
        {
            Age = 18;
            Name = "S.Unknown";
            Surname = "S.Unknownyan";
            Email = "s.unknown@unknown.com";
        }
        private int _age;
        public int Age
        {
            set
            {
                if (value > 0 && value < 115)
                {
                    _age = value;
                }
            }
            get { return _age; }
        }
        
        private string _email;
        public string Email
        {
            get { return _email; }
            set
            {
                if (!string.IsNullOrEmpty(value) && (value.EndsWith("@gmail.com") || value.EndsWith("@mail.ru")))
                {
                    _email = value;
                }
            }
        }
    }
}
